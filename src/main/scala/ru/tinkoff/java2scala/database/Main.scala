package ru.tinkoff.java2scala.database

import java.time.LocalDate
import java.util.UUID

import ru.tinkoff.java2scala.database.db.DB.IO
import ru.tinkoff.java2scala.database.db.user.{EmailConfirmation, EmailConfirmations, User, Users}
import ru.tinkoff.java2scala.database.db.{DB, Schema}
import slick.dbio.Effect
import slick.jdbc.{GetResult, H2Profile, JdbcProfile, PositionedResult}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import slick.jdbc.H2Profile.api._
import slick.sql.SqlStreamingAction

import scala.concurrent.ExecutionContext.Implicits.global

trait DatebaseProfile {
  val profile: JdbcProfile
}

object Main extends App with H2Profile  {
  Await.ready(Schema.run(), Duration.Inf)

  println(Await.ready(DB.db.run(Users.All.result), Duration.Inf))


  private val usersWithLongName: Query[Users, User, Seq] = Users.All.filter(_.name.length > 20)

  val usersCount: IO[Int, Effect.Read] =
    usersWithLongName
    .filter(_.email.length > 50)
    .length
    .result

  val deleteUsersWithLongNames: IO[Int, Effect.Write] = usersWithLongName.delete

  val countAndDelete: IO[Int, Effect.Read with Effect.Write] =
    for {
      count <- usersWithLongName.length.result
      _ <- deleteUsersWithLongNames
    } yield count


  val result: IO[Seq[Boolean], Effect.Read] = EmailConfirmations.All
    .filter(_.userId.in(usersWithLongName.map(_.id)))
    .map(_.confirmed)
    .result

  def insertUser(user: User): IO[Int, Effect.Write] = Users.All += user
  def insertConfirmation(emailConfirmation: EmailConfirmation): IO[Int, Effect.Write] = EmailConfirmations.All += emailConfirmation

  def registerUser(user: User): IO[Unit, Effect.Write with Effect.Transactional] = {
    (for {
      _ <- insertUser(user)
      confirmation = EmailConfirmation(user.id, LocalDate.now(), false, None)
      _ <- insertConfirmation(confirmation)
    } yield ()).transactionally
  }


  val res: IO[Seq[(EmailConfirmation, Option[User])], Effect.Read] =
    EmailConfirmations.All
    .joinLeft(Users.All)
    .on(_.userId === _.id)
    .result

  implicit val getUserResult: GetResult[User] = (v1: PositionedResult) => {
    User(
      UUID.fromString(v1.nextString()),
      v1.nextString(),
      v1.nextString()
    )
  }

  val query: IO[Vector[User], Effect] = sql"""select id, name , email from USERS""".as[User]

}

// thread pool
// FRM vs ORM

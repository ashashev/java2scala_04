package ru.tinkoff.java2scala.database.db

import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._

object DB {
  val db = Database.forConfig("h2mem1")

  type IO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
}
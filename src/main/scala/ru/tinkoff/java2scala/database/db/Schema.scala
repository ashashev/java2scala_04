package ru.tinkoff.java2scala.database.db

import java.util.UUID

import ru.tinkoff.java2scala.database.db.user.{User, Users}
import slick.dbio.Effect

import scala.concurrent.Future

// Только в целях иллюстрации. В боевом коде так лучше не делать.
import concurrent.ExecutionContext.Implicits.global
import DB._

object Schema {
  import slick.jdbc.H2Profile.api._ // Для ДЕМО используем H2

  val createTestUser =
    Users.All ++= Seq(User(UUID.randomUUID(), "Миша", "mishka@123.com")
    )


  def run(): Future[Unit] = {
    val createTable = Users.All.schema.create

    val query = createTable
      .andThen(createTestUser)
      .transactionally

    db.run(query)
  }.map(_ => ())
}

package ru.tinkoff.java2scala.database.db.user

import java.time.LocalDate
import java.util.UUID

import slick.jdbc.H2Profile.api._
import slick.lifted.MappedProjection // Для ДЕМО используем H2

case class EmailConfirmation(userId: UUID, sentAt: LocalDate, confirmed: Boolean, confirmedAt: Option[LocalDate])

class EmailConfirmations(tag: Tag) extends Table[EmailConfirmation](tag, "EMAIL_CONFIRMATIONS") {
  def userId: Rep[UUID] = column("USER_ID", O.PrimaryKey)
  def confirmedAt: Rep[Option[LocalDate]] = column("CONFIRMED_AT")
  def confirmed: Rep[Boolean] = column("CONFIRMED")
  def sentAt: Rep[LocalDate] = column("SENT_AT")

  override def * = (userId, sentAt, confirmed, confirmedAt).mapTo[EmailConfirmation]
}


object  EmailConfirmations {
  val All = TableQuery[EmailConfirmations]
}
package ru.tinkoff.java2scala.database.db.user

import java.time.LocalDate
import java.util.UUID

import slick.jdbc.H2Profile.api._ // Для ДЕМО используем H2

case class User(id: UUID,
                name: String,
                email: String)

case class UserInfo(name: String, email: String)

class Users(tag: Tag) extends Table[User](tag, "USERS") {
  def id: Rep[UUID] = column("ID", O.PrimaryKey)
  def name: Rep[String] = column("NAME")
  def email: Rep[String] = column("EMAIL")

  def info = (name, email).mapTo[UserInfo]

  override def * = (id, name, email).mapTo[User]
}

object Users {
  val All = TableQuery[Users]

}